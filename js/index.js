$(document).ready(function() {
  $('#mostrarTodos').click(users);
});
/*
  Creación de una función personalizada para jQuery que detecta cuando se detiene el scroll en la página
*/
$.fn.scrollEnd = function(callback, timeout) {
  $(this).scroll(function(){
    var $this = $(this);
    if ($this.data('scrollTimeout')) {
      clearTimeout($this.data('scrollTimeout'));
    }
    $this.data('scrollTimeout', setTimeout(callback,timeout));
  });
};
/*
  Función que inicializa el elemento Slider
*/

function inicializarSlider(){
  $("#rangoPrecio").ionRangeSlider({
    type: "double",
    grid: false,
    min: 0,
    max: 100000,
    from: 200,
    to: 80000,
    prefix: "$"
  });
}
/*
  Función que reproduce el video de fondo al hacer scroll, y deteiene la reproducción al detener el scroll
*/
function playVideoOnScroll(){
  var ultimoScroll = 0,
      intervalRewind;
  var video = document.getElementById('vidFondo');
  $(window)
    .scroll((event)=>{
      var scrollActual = $(window).scrollTop();
      if (scrollActual > ultimoScroll){
       video.play();
     } else {
        //this.rewind(1.0, video, intervalRewind);
        video.play();
     }
     ultimoScroll = scrollActual;
    })
    .scrollEnd(()=>{
      video.pause();
    }, 10)
}

function users(){
  $.ajax({
    url: "library.php",
    dataType: "json",
    cache: false,
    contentType: false,
    processData: false,
    type: 'get',
    success: function(response){
      $('#items').empty();
      $.each(response,function(index, value){
        $('#items').append('<div class="tituloContenido">'+
                              '<div class="row card">'+
                               '<div class="col s6"><img src="./img/home.jpg" width="100%" stylee="margin: 10px;"></div>'+
                                '<div class="col s6">'+
                                  '<h6>Direccion: <span>'+value['Direccion']+'</span></h6>'+
                                  '<h6>Ciudad: <span>'+value['Ciudad']+'</span></h6>'+
                                  '<h6>Telefono: <span>'+value['Telefono']+'</span></h6>'+
                                  '<h6>Codigo postal: <span>'+value['Codigo_Postal']+'</span></h6>'+
                                  '<h6>Tipo: <span>'+value['Tipo']+'</span></h6>'+
                                  '<h6>Precio: <span style="color: orange;">'+value['Precio']+'</span></h6>'+
                                '</div>'+
                              '</div>'+
                            '</div>');
      });

    }
  })
}

function initSelect(){
  var select1 = [];
  var select1 = [];
  var cont1 = 1;
  var cont2 = 1;
  $.ajax({
    url: "library.php",
    dataType: "json",
    cache: false,
    contentType: false,
    processData: false,
    type: 'get',
    success: function(response){
      $('#selectCiudad').empty();
      $('#selectCiudad').append('<option value="" selected>Elige una ciudad</option>');
      $('#selectTipo').empty();
      $('#selectTipo').append('<option value="" selected>Elige un tipo</option>');
      $.each(response,function(index, value){
        for (var i = 0; i <= cont1; i++) {
          if (!select1.includes(value['Ciudad'])) {
            select1[cont1] = value['Ciudad'];
            $('#selectCiudad').append('<option value="'+value['Ciudad']+'">'+value['Ciudad']+'</option>');
            cont1++;
          }
        }

        for (var i = 0; i <= cont2; i++) {
          if (!select1.includes(value['Tipo'])) {
            select1[cont1] = value['Tipo'];
            $('#selectTipo').append('<option value="'+value['Tipo']+'">'+value['Tipo']+'</option>');
            cont1++;
          }
        }
        
      });

    }
  })
}

function find(){
  var ciudad  = $('#selectCiudad').val();
  var tipo    = $('#selectTipo').val();
  var rango   = $('#rangoPrecio').val();
  var price      = rango.split(';');
  console.log(price[0] + ' y ' + price[1]);
  $.ajax({
    url: "library.php",
    dataType: "json",
    cache: false,
    contentType: false,
    processData: false,
    type: 'get',
    success: function(response){
      $('#items').empty();
      $.each(response,function(index, value){

        var precio = value['Precio'].split('$');
        precio[1] = precio[1].replace(/,/g, "");

          if (ciudad.length > 0 && tipo.length == 0) {
            console.log('ciudad');
            
            if ( ciudad == value['Ciudad'] && (parseInt(precio[1]) >= parseInt(price[0]) && parseInt(precio[1]) <= parseInt(price[1])) ) {
              $('#items').append('<div class="tituloContenido">'+
                                '<div class="row card">'+
                                 '<div class="col s6"><img src="./img/home.jpg" width="100%" stylee="margin: 10px;"></div>'+
                                  '<div class="col s6">'+
                                    '<h6>Direccion: <span>'+value['Direccion']+'</span></h6>'+
                                    '<h6>Ciudad: <span>'+value['Ciudad']+'</span></h6>'+
                                    '<h6>Telefono: <span>'+value['Telefono']+'</span></h6>'+
                                    '<h6>Codigo postal: <span>'+value['Codigo_Postal']+'</span></h6>'+
                                    '<h6>Tipo: <span>'+value['Tipo']+'</span></h6>'+
                                    '<h6>Precio: <span style="color: orange;">'+value['Precio']+'</span></h6>'+
                                  '</div>'+
                                '</div>'+
                              '</div>');
            }  
          }

          if (ciudad.length == 0 && tipo.length > 0) {
            console.log('tipo');
            if (tipo == value['Tipo'] && (parseInt(precio[1]) >= parseInt(price[0]) && parseInt(precio[1]) <= parseInt(price[1]))) {
              $('#items').append('<div class="tituloContenido">'+
                                '<div class="row card">'+
                                 '<div class="col s6"><img src="./img/home.jpg" width="100%" stylee="margin: 10px;"></div>'+
                                  '<div class="col s6">'+
                                    '<h6>Direccion: <span>'+value['Direccion']+'</span></h6>'+
                                    '<h6>Ciudad: <span>'+value['Ciudad']+'</span></h6>'+
                                    '<h6>Telefono: <span>'+value['Telefono']+'</span></h6>'+
                                    '<h6>Codigo postal: <span>'+value['Codigo_Postal']+'</span></h6>'+
                                    '<h6>Tipo: <span>'+value['Tipo']+'</span></h6>'+
                                    '<h6>Precio: <span style="color: orange;">'+value['Precio']+'</span></h6>'+
                                  '</div>'+
                                '</div>'+
                              '</div>');
            }  
          }

          if (ciudad.length > 0 && tipo.length > 0) {
            console.log('ambos');
            if (tipo == value['Tipo'] && ciudad == value['Ciudad'] && (parseInt(precio[1]) >= parseInt(price[0]) && parseInt(precio[1]) <= parseInt(price[1]))) {
              $('#items').append('<div class="tituloContenido">'+
                                '<div class="row card">'+
                                 '<div class="col s6"><img src="./img/home.jpg" width="100%" stylee="margin: 10px;"></div>'+
                                  '<div class="col s6">'+
                                    '<h6>Direccion: <span>'+value['Direccion']+'</span></h6>'+
                                    '<h6>Ciudad: <span>'+value['Ciudad']+'</span></h6>'+
                                    '<h6>Telefono: <span>'+value['Telefono']+'</span></h6>'+
                                    '<h6>Codigo postal: <span>'+value['Codigo_Postal']+'</span></h6>'+
                                    '<h6>Tipo: <span>'+value['Tipo']+'</span></h6>'+
                                    '<h6>Precio: <span style="color: orange;">'+value['Precio']+'</span></h6>'+
                                  '</div>'+
                                '</div>'+
                              '</div>');
            }  
          }

          if (ciudad.length == 0 && tipo.length == 0) {
            console.log('ambos vacios');
            if (parseInt(precio[1]) >= parseInt(price[0]) && parseInt(precio[1]) <= parseInt(price[1])) {
              $('#items').append('<div class="tituloContenido">'+
                                '<div class="row card">'+
                                 '<div class="col s6"><img src="./img/home.jpg" width="100%" stylee="margin: 10px;"></div>'+
                                  '<div class="col s6">'+
                                    '<h6>Direccion: <span>'+value['Direccion']+'</span></h6>'+
                                    '<h6>Ciudad: <span>'+value['Ciudad']+'</span></h6>'+
                                    '<h6>Telefono: <span>'+value['Telefono']+'</span></h6>'+
                                    '<h6>Codigo postal: <span>'+value['Codigo_Postal']+'</span></h6>'+
                                    '<h6>Tipo: <span>'+value['Tipo']+'</span></h6>'+
                                    '<h6>Precio: <span style="color: orange;">'+value['Precio']+'</span></h6>'+
                                  '</div>'+
                                '</div>'+
                              '</div>');
            }  
          }
                
      });

    }
  })
}

inicializarSlider();
initSelect();